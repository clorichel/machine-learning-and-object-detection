import argparse
import os.path
import sys
import json
import re
from six.moves import urllib

PIXABAY_API_HITS_LIMIT = 500
PER_PAGE = 200
FLAGS = None
erase_line = '\x1b[2K'

def check_for_positive_int(value):
  int_value = int(value)
  if int_value <= 0:
    raise argparse.ArgumentTypeError("%s is not a positive value" % value)
  return int_value

def main():
  print('# Download Pixabay images', '\n')
  print(str(len(FLAGS.categories)) + ' categories to iterate on: ', FLAGS.categories, '\n')
  total_images_downloaded = 0
  for category in FLAGS.categories:
    print('## Category: ' + category, '\n')
    category_images_downloaded = 0
    skip_next_pages = False
    dest_dir = os.path.join(FLAGS.downloaded_images_dir, category)
    if not os.path.exists(dest_dir):
      os.makedirs(dest_dir)
    for page in range(1, (FLAGS.pages + 1)):
      if skip_next_pages:
        print('\n**Skipping next page(s)**: all available images already downloaded.')
        break # move to next category
      print('- API page ' + str(page) + '/' + str(FLAGS.pages) + ': ')
      api_url = 'https://pixabay.com/api/?key=' + FLAGS.api_key + '&per_page=' + str(PER_PAGE) + '&page=' + str(page) + '&q=' + urllib.parse.quote(category);
      try:
        response = urllib.request.urlopen(api_url).read()
      except BaseException as e:
        print('  **Pixabay API error** on page ' + str(page) + ' for ' + category)
        print('\n```\n', e, '\n```\n')
        continue # move to next page
      pixabay_response = json.loads(response)
      print('  **' + str(len(pixabay_response['hits'])) + ' API results** out of ' + str(pixabay_response['total']) + ' total on the website')
      page_images_downloaded = 1
      page_images_to_download = len(pixabay_response['hits'])
      for hit in pixabay_response['hits']:
        filename = hit['pageURL'].replace(r'https://pixabay.com/en/', r'')
        filename = filename[:-1] + '.' + re.search('.*\\.(.+?)$', hit['largeImageURL']).group(1)
        filepath = os.path.join(dest_dir, filename)
        filepath, _ = urllib.request.urlretrieve(hit['largeImageURL'], filepath)
        sys.stdout.write('%s\r  > Downloading image %s/%s (%.1f%%) %s' % (erase_line, page_images_downloaded, page_images_to_download, float(page_images_downloaded) / float(page_images_to_download) * 100.0, filename))
        sys.stdout.flush()
        page_images_downloaded += 1
        category_images_downloaded += 1
        total_images_downloaded += 1
      print()
      if category_images_downloaded >= pixabay_response['totalHits']:
        skip_next_pages = True
    print('\n' + str(category_images_downloaded) + ' images downloaded for ' + category + '.\n')
  print('\n## Downloading report\n\n' + str(total_images_downloaded) + ' total images downloaded.')

if __name__ == '__main__':
  parser = argparse.ArgumentParser()
  parser.add_argument(
    '--api_key',
    type=str,
    required=True,
    help='<Required> Your Pixabay API key.'
  )
  parser.add_argument(
    '--categories',
    metavar='CATEGORY',
    type=str,
    nargs='+',
    default=['ferry boat', 'sailboat'], #, 'freight boat', 'inflatable boat', 'cruise ship', 'gondola', 'kayak', 'buoy', 'paper boat'],
    help="""\
    List of categories to download images for, separated by spaces. Use quotes
    to have spaces in a category, for example 'kayak "ferry boat" gondola'.\
    """
  )
  parser.add_argument(
    '--pages',
    type=check_for_positive_int,
    default='3',
    help="""\
    Number of results pages to use from the Pixabay API. Must be postive, note
    that there are """ + str(PER_PAGE) + """ images per page, and the API is
    limited to return a maximum of """ + str(PIXABAY_API_HITS_LIMIT) + """
    results per query.\
    """
  )
  parser.add_argument(
    '--downloaded_images_dir',
    type=str,
    default='downloaded_images',
    help='Path to store downloaded images.'
  )
  FLAGS, unparsed = parser.parse_known_args()
  main()