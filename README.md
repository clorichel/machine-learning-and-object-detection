# Machine learning and object detection

This repository contains the code base used on this blog post https://clorichel.com/blog/2018/11/10/machine-learning-and-object-detection/ where you'll train an image recognition model with TensorFlow to find about anything on pictures and videos.

## Credits

- the `retrain.py` and `classify_image.py` scripts by [The TensorFlow Authors](https://www.tensorflow.org/) have been adjusted and tweaked
- gondola photo by [Jack Ward](https://unsplash.com/photos/bISYrjlXqIs?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText) on [Unsplash](https://unsplash.com/search/photos/gondola?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText)
- cruise ship photo by [Mike Arney](https://unsplash.com/photos/yRyZzxolfSs?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText) on [Unsplash](https://unsplash.com/search/photos/gondola?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText)
- kayak photo by [Kalen Emsley](https://unsplash.com/photos/NrN5Rvl89Lo?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText) on [Unsplash](https://unsplash.com/search/photos/gondola?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText)